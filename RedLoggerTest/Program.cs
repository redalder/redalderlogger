﻿using System;
using System.IO;
using RedLogger;

namespace RedLoggerTest
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            FileStream file = File.Open("aaa.txt", FileMode.Create, FileAccess.Write);
            FileStream file2 = File.Open("bbb.txt", FileMode.Create, FileAccess.Write);
            
            Log log = new Log(Console.Out, "[{file_name}:{line_number}] {level}: {text}\n");
            log.Pipe = new Pipe
            {
                Condition = delegate (Message message, Log lhs, Log rhs) {
                    if (message.Level.Value == Level.Info || message.Level.Value == Level.Warning ||
                        message.Level.Value == Level.Error)
                    {
                        return true;
                    }

                    return false;
                },
                Enabled = true,
                RhsLog = new Log(file, log.Format, 0)
            };

            log.Write(Level.Info, DateTime.Now.ToLongTimeString());
            log.Write(Level.Warning, DateTime.Now.ToLongDateString());
            log.Write(Level.Error, DateTime.Now.ToShortTimeString());
            log.Write(Level.CriticalError, DateTime.Now.ToShortDateString());
            
            log.Dispose();
        }
    }
}