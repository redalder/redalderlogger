using System.Collections.Generic;

namespace RedLogger
{
    public class Message
    {
        public KeyValuePair<string, string> FileName = new KeyValuePair<string, string>("{file_name}", string.Empty);
        public KeyValuePair<string, string> FilePath = new KeyValuePair<string, string>("{file_path}", string.Empty);
        public KeyValuePair<string, string> LineNumber = new KeyValuePair<string, string>("{line_number}", string.Empty);
        public KeyValuePair<string, Level> Level = new KeyValuePair<string, Level>("{level}", RedLogger.Level.Info);
        public KeyValuePair<string, string> Text = new KeyValuePair<string, string>("{text}", string.Empty);
    }
}