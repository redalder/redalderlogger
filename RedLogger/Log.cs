using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RedLogger
{
    public class Log : IDisposable
    {
        public Pipe Pipe;
        public readonly long RotationSize;
        public string Format { get; }
        public long Position
        {
            get { return _stream != null ? -1 : _stream.Position; }
        }

        private readonly TextWriter _textWriter;
        private readonly Stream _stream;
        
        public Log(TextWriter textWriter, string format)
        {
            Format = format;

            _textWriter = textWriter;
        }
        
        public Log(Stream stream, string format, long rotationSize = 0)
        {
            Format = format;
            RotationSize = rotationSize;
            _stream = stream;
        }

        public void Dispose()
        {
            if (Pipe.Enabled)
                Pipe.RhsLog.Dispose();
            
            _textWriter?.Close();
            _stream?.Close();
        }

        public void Write(Level level, string text)
        {
            Message msg = new Message();

            string fileName = new System.Diagnostics.StackTrace(true).GetFrame(1).GetFileName();
            fileName = fileName.Substring(fileName.LastIndexOf('/') + 1);

            string filePath = new System.Diagnostics.StackTrace(true).GetFrame(1).GetFileName();
            int lineNumber = new System.Diagnostics.StackTrace(true).GetFrame(1).GetFileLineNumber(); 
            
            msg.FileName = new KeyValuePair<string, string>(msg.FileName.Key, fileName);
            msg.FilePath = new KeyValuePair<string, string>(msg.FilePath.Key, filePath);
            msg.LineNumber = new KeyValuePair<string, string>(msg.LineNumber.Key, Convert.ToString(lineNumber));
            msg.Level = new KeyValuePair<string, Level>(msg.Level.Key, level);
            msg.Text = new KeyValuePair<string, string>(msg.Text.Key, text);

            DispatchWrite(msg);

            if (!Pipe.Enabled)
                return;
            
            if (Pipe.Condition(msg, this, Pipe.RhsLog))
            {
                Pipe.RhsLog.Write(msg);
            }
        }

        private void Write(Message msg)
        {
            DispatchWrite(msg);

            if (!Pipe.Enabled)
                return;
            if (Pipe.Condition(msg, this, Pipe.RhsLog))
            {
                Pipe.RhsLog.Write(msg);
            }
        }

        private void DispatchWrite(Message msg)
        {
            
            _textWriter?.Write(_parseFormat(msg));
            if (_stream != null)
            {
                if (_stream.Position > RotationSize && RotationSize != 0)
                    _stream.SetLength(0);
                _stream.Write(Encoding.ASCII.GetBytes(_parseFormat(msg)));
            }
        }
        
        private string _parseFormat(Message message)
        {
            return Format
                .Replace(message.FileName.Key, message.FileName.Value)
                .Replace(message.FilePath.Key, message.FilePath.Value)
                .Replace(message.LineNumber.Key, message.LineNumber.Value)
                .Replace(message.Level.Key, message.Level.Value.ToString())
                .Replace(message.Text.Key, message.Text.Value);
        }
    }
}