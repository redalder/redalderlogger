namespace RedLogger
{
    public enum Level
    {
        Info,
        Warning,
        Error,
        CriticalError
    }
}