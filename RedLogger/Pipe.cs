namespace RedLogger
{
    public delegate bool Condition(Message message, Log lhs, Log rhs);
    
    public struct Pipe
    {
        public bool Enabled;
        public Log RhsLog;
        public Condition Condition;
    }
}